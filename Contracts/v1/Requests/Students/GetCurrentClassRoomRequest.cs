﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DSID.Demo.Api.Contracts.v1.Requests.Students
{
    public class GetCurrentClassRoomRequest
    {
        public bool async { get; set; }
    }
}
