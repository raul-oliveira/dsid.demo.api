﻿using AutoMapper;
using DSID.Demo.Api.Models.Domain;
using DSID.Demo.Api.Repositories.Students;
using DSID.Demo.Api.Services.InfraStructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DSID.Demo.Api.Services
{
    public class StudentService
    {
        private readonly StudentRepository _studentRepository;
        private readonly ClassRoomService _classRoomService;
        private readonly IMapper _mapper;

        public StudentService(
            StudentRepository studentRepository,
            ClassRoomService classRoomService,
            IMapper mapper)
        {
            _studentRepository = studentRepository;
            _classRoomService = classRoomService;
            _mapper = mapper;
        }

        public async Task<bool> CreateStudentAsync(Student student)
            => await _studentRepository.CreateStudentAsync(student);

        public async Task<bool> DeleteStudentAsync(int id)
            => await _studentRepository.DeleteStudentAsync(id);

        public async Task<Student> GetStudentByIdAsync(int id)
            => await _studentRepository.GetStudentByIdAsync(id);

        public async Task<List<Student>> GetStudentsAsync()
            => await _studentRepository.GetStudentsAsync();

        public async Task<bool> UpdateStudentAsync(Student studentToUpdate)
            => await _studentRepository.UpdateStudentAsync(studentToUpdate);

        public async Task<ClassRoom> GetCurrentClassRoomAsync(bool async = true)
        {
            // Exemplo para a apresentação de DSID
            // TEMA: Processamento paralelo em servidores diferentes
            // CASO 1: Paralelo
            // CASO 2: Normal

            var startDate = DateTime.Now;

            if (async)
            {
                var classRoomResponse = _classRoomService.GetCurrentClassRoomInfoAsync();
                var studentsResponse = _classRoomService.GetCurrentClassRoomStudentsAsync();

                Task.WaitAll(new Task[] { classRoomResponse, studentsResponse });

                var classRoom = _mapper.Map<ClassRoom>(classRoomResponse.Result);
                classRoom.Students = await _studentRepository.GetStudentsByIdsAsync(studentsResponse.Result.Students);

                classRoom.ProcessEnded = DateTime.Now;
                classRoom.ProcessStarted = startDate;
                classRoom.TotalTime = (classRoom.ProcessEnded - classRoom.ProcessStarted).Seconds.ToString();
                return classRoom;
            } 
            else
            {
                var classRoomResponse = await _classRoomService.GetCurrentClassRoomInfoAsync();
                var studentsResponse = await _classRoomService.GetCurrentClassRoomStudentsAsync();

                var classRoom = _mapper.Map<ClassRoom>(classRoomResponse);
                classRoom.Students = await _studentRepository.GetStudentsByIdsAsync(studentsResponse.Students);

                classRoom.ProcessEnded = DateTime.Now;
                classRoom.ProcessStarted = startDate;
                classRoom.TotalTime = (classRoom.ProcessEnded - classRoom.ProcessStarted).Seconds.ToString();
                return classRoom;
            }


        }
    }
}
