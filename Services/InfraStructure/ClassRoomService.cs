﻿using DSID.Demo.Api.Models.InfraStructure;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace DSID.Demo.Api.Services.InfraStructure
{
    public class ClassRoomService
    {
        public async Task<ClassRoomInfo> GetCurrentClassRoomInfoAsync ()
        {
            var client = new HttpClient();
            var response = await client.PostAsJsonAsync("https://localhost:44376/ClassRoom/info", new { });
            return JsonConvert.DeserializeObject<ClassRoomInfo>(await response.Content.ReadAsStringAsync());
        }

        public async Task<ClassRoomStudents> GetCurrentClassRoomStudentsAsync()
        {
            var client = new HttpClient();
            var response = await client.PostAsJsonAsync("https://localhost:44376/ClassRoom/students", new { });
            return JsonConvert.DeserializeObject<ClassRoomStudents>(await response.Content.ReadAsStringAsync());
        }
    }
}
