﻿using Microsoft.EntityFrameworkCore;
using DSID.Demo.Api.Data;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DSID.Demo.Api.Models;
using DSID.Demo.Api.Models.DTO;
using System.Data.SqlClient;
using DSID.Demo.Api.Models.Domain;
using AutoMapper;
using System.Linq;

namespace DSID.Demo.Api.Repositories.Students
{
    /// <summary>
    /// Class that contains the business of Students
    /// </summary>
    public class StudentRepository
    {
        private readonly DataContext _dataContext;
        private readonly IMapper _mapper;

        /// <summary>
        /// Dependencies of StudentRepository
        /// </summary>
        public StudentRepository(DataContext dataContext, IMapper mapper)
        {
            _dataContext = dataContext;
            _mapper = mapper;
        }
        
        /// <summary>
        /// Create a Student
        /// </summary>
        /// <param name="Student"></param>
        /// <returns></returns>
        public async Task<bool> CreateStudentAsync(Student student)
        {
            var dto = _mapper.Map<StudentDto>(student);
            await _dataContext.Students.AddAsync(dto);
            var created = await _dataContext.SaveChangesAsync();
            student.Id = dto.Id;
            return created > 0;
        }
        
        /// <summary>
        /// Delete a Student by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<bool> DeleteStudentAsync(int id)
        {
            var student = await GetStudentByIdAsync(id);

            if (student == null)
                return false;

            _dataContext.Students.FromSql(string.Format("delete from students where Id = {0}", id));
            return true;
        }

        /// <summary>
        /// Get a Student by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Student> GetStudentByIdAsync(int id)
            => _mapper.Map<Student>(await _dataContext.Students.FirstOrDefaultAsync(x => x.Id == id));

        /// <summary>
        /// list all Students
        /// </summary>
        /// <returns></returns>
        public async Task<List<Student>> GetStudentsAsync()
            => _mapper.Map<List<StudentDto>, List<Student>>(await _dataContext.Students.ToListAsync());

        /// <summary>
        /// Update a Student
        /// </summary>
        /// <param name="StudentToUpdate"></param>
        /// <returns></returns>
        public async Task<bool> UpdateStudentAsync(Student StudentToUpdate)
        {
            var dto = _mapper.Map<StudentDto>(StudentToUpdate);
            _dataContext.Students.Update(dto);
            var updated = await _dataContext.SaveChangesAsync();
            return updated > 0;
        }

        /// <summary>
        /// Get students by a list of ids
        /// </summary>
        /// <param name="Student"></param>
        /// <returns></returns>
        public async Task<List<Student>> GetStudentsByIdsAsync(List<int> studentIds)
        {
            var students = (await _dataContext.Students.ToListAsync())
                .Join(studentIds, student => student.Id, x => x, (student, id) => student).ToList();

            return _mapper.Map<List<StudentDto>, List<Student>>(students);
        }
    }
}
