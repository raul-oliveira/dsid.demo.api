﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using DSID.Demo.Api.Models.DTO;

namespace DSID.Demo.Api.Data
{
    /// <summary>
    /// Performs the tables and relations
    /// </summary>
    public class DataContext : IdentityDbContext
    {
        /// <summary>
        /// Initialize the base class and all dependencies
        /// </summary>
        /// <param name="options"></param>
        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {
        }

        public DbSet<StudentDto> Students { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }

    }
}
