﻿namespace DSID.Demo.Api.Options
{
    /// <summary>
    /// Class SwaggerOptions
    /// </summary>
    public class SwaggerOptions
    {
        /// <summary>
        /// Property JsonRoute
        /// </summary>
        public string JsonRoute { get; set; }
        /// <summary>
        /// Property Description
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// Property UIEndpoint
        /// </summary>
        public string UIEndpoint { get; set; }
    }
}
