﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DSID.Demo.Api.Extensions
{
    public static class StringExtensions
    {
        public static string ExtractNumbers(this string input)
        {
            if (string.IsNullOrEmpty(input))
            {
                return "";
            }
            var numberExtractRegex = new Regex(@"\d+");
            var matches = numberExtractRegex.Matches(input);
            var result = "";
            matches.ToList().ForEach(x => result += x.Value);

            return result;
        }
    }
}
