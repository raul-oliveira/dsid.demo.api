﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DSID.Demo.Api.Models.Domain
{
    public class ClassRoom
    {
        public string ClassName { get; set; }
        public string BeginHour { get; set; }
        public string EndHour { get; set; }
        public string ProfessorName { get; set; }
        public DateTime ProcessStarted { get; set; }
        public DateTime ProcessEnded { get; set; }
        public string TotalTime { get; set; }
        public List<Student> Students { get; set; }
    }
}
