using System;

namespace DSID.Demo.Api.Models
{
    /// <summary>
    /// Class ErrorViewModel
    /// </summary>
    public class ErrorViewModel
    {

        /// <summary>
        /// property RequestId
        /// </summary>
        public string RequestId { get; set; }
        /// <summary>
        /// Property ShowRequestId 
        /// </summary>
        public bool ShowRequestId => !string.IsNullOrEmpty(RequestId);
    }
}