﻿using DSID.Demo.Api.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DSID.Demo.Api.Models.InfraStructure
{
    public class ClassRoomStudents
    {
        public List<int> Students { get; set; }
    }
}
