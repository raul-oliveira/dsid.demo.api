﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DSID.Demo.Api.Models.InfraStructure
{
    public class ClassRoomInfo
    {
        public string ClassName { get; set; }
        public string BeginHour { get; set; }
        public string EndHour { get; set; }
        public string ProfessorName { get; set; }
    }
}
