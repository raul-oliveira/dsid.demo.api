﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DSID.Demo.Api.Maps
{
    public class ClassRoomMaps : Profile
    {
        public ClassRoomMaps()
        {
            CreateMap<Models.InfraStructure.ClassRoomInfo, Models.Domain.ClassRoom>();
        }
    }
}
