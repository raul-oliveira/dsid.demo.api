﻿using AutoMapper;
using DSID.Demo.Api.Contracts.v1.Requests.Students;
using DSID.Demo.Api.Models.Domain;
using DSID.Demo.Api.Models.DTO;

namespace DSID.Demo.Api.Maps
{
    public class StudentMaps : Profile
    {
        public StudentMaps ()
        {
            CreateMap<StudentDto, Student>();
            CreateMap<Student, StudentDto>();

            CreateMap<UpdateRequest, Student>();
            CreateMap<CreateRequest, Student>();
        }
    }
}
