--------Welcome to the Landim Veiculos project--------

Entity Framework usefull commands:

What: Add-migration command generates the migration files that your project will use to update the database.
Command: Add-Migration "[Description]"
Sample: Add-Migration "YourChangeOnTheDb"


What: Update-Database command send to the database all your changes in the db context.
command: Update-Database

NOTE: Package manager console to input these commands.
