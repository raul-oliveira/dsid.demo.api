﻿using AutoMapper;
using DSID.Demo.Api.Contracts.v1.Requests.Students;
using DSID.Demo.Api.Models.Domain;
using DSID.Demo.Api.Models.DTO;
using DSID.Demo.Api.Services;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DSID.Demo.Api.Controllers.v1
{
    /// <summary>
    /// Students Controller
    /// </summary>
    [Route("api/v1/[controller]")]
    public class StudentController : Controller
    {
        /// <summary>
        /// Students service class
        /// </summary>
        private readonly StudentService _studentServices;

        /// <summary>
        /// Auto mapper autoDi instance
        /// </summary>
        private readonly IMapper _mapper;

        /// <summary>
        /// Constructor that contains the dependencys injections
        /// </summary>
        public StudentController(
            StudentService studentService,
            IMapper mapper)
        {
            _studentServices = studentService;
            _mapper = mapper;
        }

        /// <summary>
        /// List all the Students
        /// </summary>
        /// <returns>List of Students</returns>
        [HttpGet]
        [Route("")]
        public async Task<ActionResult<IEnumerable<Student>>> GetAll()
        {
            return Ok(await _studentServices.GetStudentsAsync());
        }

        /// <summary>
        /// Update a Student
        /// </summary>
        /// <returns>If its successfully update a Student, return the request body, else returns 404</returns>
        [HttpPut]
        [Route("{studentId}")]
        public async Task<ActionResult> Update([FromRoute]int studentId, [FromBody] UpdateRequest request)
        {
            var student = _mapper.Map<Student>(request);
            student.Id = studentId;
            var updated = await _studentServices.UpdateStudentAsync(student);

            if (updated)
            {
                return Ok(student);
            }
            else
            {
                return NotFound();
            }
        }

        /// <summary>
        /// Get a Student by StudentId
        /// </summary>
        /// <param name="StudentId">key of Student entity</param>
        /// <returns></returns>
        [HttpGet]
        [Route("{studentId}")]
        public async Task<ActionResult<Student>> GetById([FromRoute]int studentId)
        {
            var student = await _studentServices.GetStudentByIdAsync(studentId);
            if (student == null)
            {
                return NotFound();
            }
            return Ok(student);
        }

        /// <summary>
        /// Creates a Student
        /// </summary>
        /// <param name="request">new Student to be added</param>
        /// <returns></returns>
        [HttpPost]
        [Route("")]
        public async Task<ActionResult<Student>> Create([FromBody] CreateRequest request)
        {
            var student = _mapper.Map<Student>(request);
            await _studentServices.CreateStudentAsync(student);
            
            return CreatedAtAction(nameof(GetById), new { studentId = student.Id }, request);

        }

        /// <summary>
        /// Delete a Student
        /// </summary>
        /// <param name="StudentId">key of Student entity</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("{studentId}")]
        public async Task<IActionResult> Delete([FromRoute] int studentId)
        {
            var deleted = await _studentServices.DeleteStudentAsync(studentId);
            if (deleted)
            {
                return NoContent();
            }
            else
            {
                return NotFound();
            }
        }

        /// <summary>
        /// Get the information about the current class and the students inside it
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("currentClass")]
        public async Task<ActionResult<ClassRoom>> GetCurrentClassRoom([FromQuery] GetCurrentClassRoomRequest request)
        {
            var classRoom = await _studentServices.GetCurrentClassRoomAsync(request.async);

            if (classRoom == null)
            {
                return NoContent();
            }

            return Ok(classRoom);
        }
    }
}
