﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using DSID.Demo.Api.Data;

namespace DSID.Demo.Api.Installers
{
    /// <summary>
    /// Contains all methods for performing DB installation
    /// </summary>
    public class DbInstaller : IInstaller
    {
        /// <summary>
        /// Install all services
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configuration"></param>
        public void InstallServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<DataContext>(options =>
                options.UseSqlServer(
                    configuration.GetConnectionString("DefaultConnection")));
        }
    }
}
