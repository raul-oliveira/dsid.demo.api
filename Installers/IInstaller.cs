﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace DSID.Demo.Api.Installers
{
    /// <summary>
    /// Contains all methods for performing installation of services
    /// </summary>
    public interface IInstaller
    {
        /// <summary>
        /// Install all services
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configuration"></param>
        void InstallServices(IServiceCollection services, IConfiguration configuration);
    }
}
