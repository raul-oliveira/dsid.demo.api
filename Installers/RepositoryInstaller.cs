﻿using DSID.Demo.Api.Repositories.Students;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;

namespace DSID.Demo.Api.Installers
{
    /// <summary>
    /// Contains all methods for performing Services dependency injection
    /// </summary>
    public class RespositoryInstaller : IInstaller
    {
        /// <summary>
        /// Install all services
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configuration"></param>
        public void InstallServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<StudentRepository>();
        }
    }
}
