﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DSID.Demo.Api.Services;
using DSID.Demo.Api.Services.InfraStructure;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace DSID.Demo.Api.Installers
{
    public class ServiceInstaller : IInstaller
    {
        public void InstallServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<StudentService>();
            services.AddScoped<ClassRoomService>();
        }
    }
}
