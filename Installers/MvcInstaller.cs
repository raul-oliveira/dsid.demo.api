﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.PlatformAbstractions;
using Newtonsoft.Json.Serialization;
using Swashbuckle.AspNetCore.Swagger;
using System;
using System.Linq;
using System.Reflection;

namespace DSID.Demo.Api.Installers
{
    /// <summary>
    /// Contains all methods for performing MVC installation
    /// </summary>
    public class MvcInstaller : IInstaller
    {
        /// <summary>
        /// Install all services
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configuration"></param>
        public void InstallServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddMvc()
                //.AddJsonOptions(options => options.SerializerSettings.ContractResolver = new DefaultContractResolver())
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddSwaggerGen(opt =>
            {
                opt.SwaggerDoc("v1", new Info { Title = "DSID Demo - API", Version = "v1" });

                var bPath = PlatformServices.Default.Application.ApplicationBasePath;
                //opt.IncludeXmlComments(bPath + "\\DSID.Demo.Api.xml");
            });
            services.AddRouting(options => options.LowercaseUrls = true);
            services.AddAutoMapper(typeof(Startup));

        }
    }
}
